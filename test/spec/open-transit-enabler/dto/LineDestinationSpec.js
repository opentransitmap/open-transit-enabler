define(function (require) {
    
    describe('LineDestination', function() {
        
        beforeEach(function() {
            LineDestination = require('ote/dto/LineDestination');
            Line = require('ote/dto/Line'); //TODO: implement
        });
        
        describe('constructor', function() {
            
            it('should accept line and destination as values', function() {
                l = new Line(); //TODO: construct Line
                ld = new LineDestination(l, new Location({
                    lat: 20,
                    lon: 10
                }));
                expect(ld.line).toBe(l);
                expect(ld.destination.getLat()).toBe(20);
                expect(ld.destination.getLon()).toBe(10);
                
                l = new Line(); //TODO: construct Line
                ld = new LineDestination(l);
                expect(ld.line).toBe(l);
            });
            
            it('should not accept line values other than Line', function() {
                expect(function() {
                    new LineDestination();
                } ).toThrowError(TypeError);
                expect(function() {
                    new LineDestination(null);
                } ).toThrowError(TypeError);
                expect(function() {
                    new LineDestination(undefined);
                } ).toThrowError(TypeError);
                expect(function() {
                    new LineDestination("");
                } ).toThrowError(TypeError);
                expect(function() {
                    new LineDestination(new Object());
                } ).toThrowError(TypeError);
            });
            
        });
        
    });
    
});
