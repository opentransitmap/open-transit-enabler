define(function(require) {
    
    describe('Point', function() {
        
        beforeEach(function() {
            Point = require('ote/dto/Point');
        });
        
        describe('constructor', function() {
            
            it('should accept latitude and longitude as numeric values', function() {
                p = new Point(12,49);
                expect(p._lat).toBe(12);
                expect(p._lon).toBe(49);
                p = new Point(12.3,49.0);
                expect(p._lat).toBe(12.3);
                expect(p._lon).toBe(49);
            });
            
            it('should not accept values other than numbers', function() {
                expect(function() {
                    new Point("Test", "Test");
                } ).toThrowError(TypeError);
                expect(function() {
                    new Point(null, 0);
                } ).toThrowError(TypeError);
            });
            
        });
        
        describe('functions', function() {
            
            it('should return the same values as given to the constructor', function() {
                p = new Point(12,45.6);
                expect(p.getLat()).toBe(12);
                expect(p.getLon()).toBe(45.6);
            });
            
        });

    });
    
});
