define(function (require) {
    
    describe('Style', function() {
        
        beforeEach(function() {
            Style = require('ote/dto/Style');
            StyleColors = require('ote/dto/StyleColors');
        });
        
        describe('constructor', function() {
            
            it('should accept various values in args', function() {
                args = {
                    foregroundColor: StyleColors.BLACK,
                    backgroundColor: StyleColors.WHITE,
                    shape: StyleShape.CIRCLE,
                    borderColor: StyleColors.BLACK,
                    backgroundColor2: StyleColors.DKGRAY
                };
                s = new Style(args);
                for (i=0; i<Object.keys(args).length; i++) {
                    arg = args[i];
                    expect(s[arg]).toBe(args[arg]);
                }
            });
            
            it('should not accept args without foreground or backgroundColor', function() {
                expect(function() {
                    s = new Style({});
                }).toThrowError(TypeError);
                expect(function() {
                    s = new Style({
                        foregroundColor: StyleColors.BLACK
                    });
                }).toThrowError(TypeError);
                expect(function() {
                    s = new Style({
                        backgroundColor: StyleColors.WHITE
                    });
                }).toThrowError(TypeError);
            });
            
        });
        
        describe('functions', function() {
            
            describe('parseColor', function() {
                
                it('should return a valid int for a colorString');
                
            });
            
        });
    });
    
});
