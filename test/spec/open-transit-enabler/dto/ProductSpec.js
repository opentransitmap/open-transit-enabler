define(function() {
    
    _ = {
        HIGH_SPEED_TRAIN: 'I',
        REGIONAL_TRAIN: 'R',
        SUBURBAN_TRAIN: 'S',
        SUBWAY: 'U',
        TRAM: 'T',
        BUS: 'B',
        FERRY: 'F',
        CABLECAR: 'C',
        ON_DEMAND: 'P'
    };
    
    //TODO: not implemented fromCode (because not necessary)
    
    return Product = _;
});
