define(function (require) {
    
    describe('Position', function() {
        
        beforeEach(function() {
            Position = require('ote/dto/Position');
        });
        
        describe('constructor', function() {
            
            it('should accept name and section as string values', function() {
                p = new Position("Platform", "A");
                expect(p.name).toBe("Platform");
                expect(p.section).toBe("A");
            });
            
            it('should not accept null names', function() {
                expect(function() {
                    new Position();
                } ).toThrowError(TypeError);
                expect(function() {
                    new Point(null);
                } ).toThrowError(TypeError);
                expect(function() {
                    new Point(undefined);
                } ).toThrowError(TypeError);
                expect(function() {
                    new Point("");
                } ).toThrowError(TypeError);
            });
            
            it('should not accept section value > 3', function() {
                expect(function() {
                    new Position("Platform", "AAAA");
                } ).toThrowError(TypeError);
            });
            
        });
        
    });
    
});
