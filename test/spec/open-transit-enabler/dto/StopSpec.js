define(function(require) {
        
        Location = require('ote/dto/Location');
        Position = require('ote/dto/Position');
        
        _ = function(args) {
                
                if ('location' in args && args.location instanceof Location) {
                        this._location = args.location; // TODO: else?
                }
                
                if ('departure' in args) {
                        if ('plannedTime' in args && args.plannedTime instanceof Date) {
                                this._plannedArrivalTime = !departure ? plannedTime : null;
                                this._plannedDepartureTime = departure ? plannedTime : null;
                        }
                        if ('predictedTime' in args && args.predictedTime instanceof Date) {
                                this._predictedArrivalTime = !departure ? predictedTime : null;
                                this._predictedDepartureTime = departure ? predictedTime : null;
                        }
                        if ('plannedPosition' in args && args.plannedPosition instanceof Date) {
                                this._plannedArrivalPosition = !departure ? plannedPosition : null;
                                this._plannedDeparturePosition = departure ? plannedPosition : null;
                        }
                        if ('predictedPosition' in args && args.predictedPosition instanceof Date) {
                                this._predictedArrivalPosition = !departure ? predictedPosition : null;
                                this._predictedDeparturePosition = departure ? predictedPosition : null;
                        }
                        this._arrivalCancelled = !departure ? (cancelled || false) : false;
                        this._departureCancelled = departure ? (cancelled || false) : false;
                } else {
                
                        if ('plannedArrivalTime' in args && args.plannedArrivalTime instanceof Date) {
                                this._plannedArrivalTime = args.plannedArrivalTime;
                        }
                        if ('predictedArrivalTime' in args && args.predictedArrivalTime instanceof Date) {
                                this._predictedArrivalTime = args.predictedArrivalTime;
                        }
                        if ('plannedArrivalPosition' in args && args.plannedArrivalPosition instanceof Position) {
                                this._plannedArrivalPosition = args.plannedArrivalPosition;
                        }
                        if ('predictedArrivalPosition' in args && args.predictedArrivalPosition instanceof Position) {
                                this._predictedArrivalPosition = args.predictedArrivalPosition;
                        }
                        this._arrivalCancelled = arrivalCancelled || false;
                        if ('plannedDepartureTime' in args && args.plannedDepartureTime instanceof Date) {
                                this._plannedDepartureTime = args.plannedDepartureTime;
                        }
                        if ('predictedDepartureTime' in args && args.predictedDepartureTime instanceof Date) {
                                this._predictedDepartureTime = args.predictedDepartureTime;
                        }
                        if ('plannedDeparturePosition' in args && args.plannedDeparturePosition instanceof Position) {
                                this._plannedDeparturePosition = args.plannedDeparturePosition;
                        }
                        if ('predictedDeparturePosition' in args && args.predictedDeparturePosition instanceof Position) {
                                this._predictedDeparturePosition = args.predictedDeparturePosition;
                        }
                        this._departureCancelled = args.departureCancelled || false;

                }               
        }
        
        _.prototype.getArrivalTime = function(preferPlanTime) {
        preferPlanTime = preferPlanTime || false;
        
        if (preferPlanTime && this._plannedArrivalTime !== null) {
            return this._plannedArrivalTime;
                } else if (this._predictedArrivalTime !== null) {
            return this._predictedArrivalTime;
        } else if (this._plannedArrivalTime !== null) {
            return this._plannedArrivalTime;
        } else {
            return null;
                }
    }

    _.prototype.isArrivalTimePredicted = function(preferPlanTime) {
        preferPlanTime = preferPlanTime || false;
        
        if (preferPlanTime && this._plannedArrivalTime !== null) {
            return false;
        } else {
            return this._predictedArrivalTime !== null;
                }
    }

    _.prototype.getArrivalDelay = function() {
        if (this._plannedArrivalTime !== null && this._predictedArrivalTime !== null) {
            return this._predictedArrivalTime.getTime() - this._plannedArrivalTime.getTime(); //TODO: check date!
        } else {
            return null;
                }
    }

    _.prototype.getArrivalPosition = function() {
        if (this._predictedArrivalPosition !== null) {
            return this._predictedArrivalPosition;
        } else if (this._plannedArrivalPosition !== null) {
            return this._plannedArrivalPosition;
        } else {
            return null;
                }
    }

    _.prototype.isArrivalPositionPredicted = function() {
        return this._predictedArrivalPosition !== null;
    }

    _.prototype.getDepartureTime = function(preferPlanTime) {
        preferPlanTime = preferPlanTime || false;
        
        if (preferPlanTime && this._plannedDepartureTime !== null) {
            return this._plannedDepartureTime;
        } else if (this._predictedDepartureTime !== null) {
            return this._predictedDepartureTime;
        } else if (this._plannedDepartureTime !== null) {
            return this._plannedDepartureTime;
        } else {
            return null;
                }
    }

    _.prototype.isDepartureTimePredicted = function(preferPlanTime) {
                preferPlanTime = preferPlanTime || false;
                
        if (preferPlanTime && this._plannedDepartureTime !== null) {
            return false;
        } else {
            return this._predictedDepartureTime !== null;
                }
    }

    _.prototype.getDepartureDelay = function() {
        if (this._plannedDepartureTime !== null && this._predictedDepartureTime !== null) {
            return this._predictedDepartureTime.getTime() - this._plannedDepartureTime.getTime(); //TODO: check date functions
        } else {
            return null;
                }
    }

    _.prototype.getDeparturePosition = function() {
        if (this._predictedDeparturePosition !== null) {
            return this._predictedDeparturePosition;
        } else if (this._plannedDeparturePosition !== null) {
            return this._plannedDeparturePosition;
        } else {
            return null;
                }
    }

    _.prototype.isDeparturePositionPredicted = function() {
        return this._predictedDeparturePosition !== null;
    }

    _.prototype.getMinTime = function() {
        if (this._plannedDepartureTime === null
                || (this._predictedDepartureTime !== null && this._predictedDepartureTime.before(this._plannedDepartureTime))) //TODO: check Date functions
            return this._predictedDepartureTime;
        else
            return this._plannedDepartureTime;
    }

    _.prototype.getMaxTime = function() {
        if (this._plannedArrivalTime === null
                || (this._predictedArrivalTime !== null && this._predictedArrivalTime.after(this._plannedArrivalTime))) //TODO: check Date functions
            return predictedArrivalTime;
        else
            return plannedArrivalTime;
    }
    
    //TODO: not implemented equals and hashCode
        
        _.prototype.toString = function() { //TODO: not implemented toString
                var string =  this._type + ',' + this._id + ',';
                if (this.hasLocation()) {
                        string += this._lat + "/" + this._lon + ',';
                }
                string += this._place + ',' + this._name + ',' + this._products;
                return string;
        };
        
        return Location = _;
});
