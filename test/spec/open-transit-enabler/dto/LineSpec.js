define(function (require) {
    
    describe('Line', function() {
        
        beforeEach(function() {
            Line = require('ote/dto/Line');
            LineAttr = require('ote/dto/LineAttr');
            Product = require('ote/dto/Product');
            Style = require('ote/dto/Style');
        });
        
        describe('constructor', function() {
            
            it('should accept various values in args', function() {
                args = {
                    id: 'line1',
                    network: 'testNetwork',
                    product: Product.HIGH_SPEED_TRAIN,
                    label: 'testLabel',
                    name: 'testName',
                    style: new Style({
                        foregroundColor: StyleColors.BLACK,
                        backgroundColor: StyleColors.WHITE
                    }),
                    attrs: [
                        LineAttr.SERVICE_REPLACEMENT,
                        LineAttr.BICYCLE_CARRIAGE
                    ],
                    message: 'testMessage'
                };
                l = new Line(args);
                for (arg in args) {
                    expect(l[arg]).toBe(args[arg]);
                }
            });
            
            it('should not accept product values other than Product', function() {
                l = new Line({
                    product: 10
                });
                expect(l.product).toBe(null);
            });
            
            it('should not accept style values other than Style', function() {
                l = new Line({
                    style: new Object()
                });
                expect(l.style).toBe(null);
            });
            
            it('should not accept attrs values other than Attr', function() {
                l = new Line({
                    attrs: [LineAttr.LINE_AIRPORT, 20]
                });
                expect(l.attrs).toBe(null);
            });
            
        });
        
        describe('functions', function () {
            
            describe('productCode()', function() {
                
                it('should return a valid Product.code or Product.UNKNOWN', function() {
                    l = new Line({
                        product: Product.HIGH_SPEED_TRAIN
                    });
                    expect(l.productCode()).toBe(Product.HIGH_SPEED_TRAIN);
                    l = new Line();
                    expect(l.productCode()).toBe(Product.UNKNOWN);
                });
                
            });
            
            describe('hasAttr()', function() {
                
                it('should return true or false', function() {
                    l = new Line({
                        attrs: [
                            LineAttr.LINE_AIRPORT,
                            LineAttr.WHEEL_CHAIR_ACCESS
                        ]
                    });
                    expect(l.hasAttr(LineAttr.WHEEL_CHAIR_ACCESS)).toBe(true);
                    expect(l.hasAttr(LineAttr.LINE_AIRPORT)).toBe(true);
                    expect(l.hasAttr(LineAttr.BICYCLE_CARRIAGE)).toBe(false);
                    
                    l = new Line({
                        attrs: [ LineAttr.BICYCLE_CARRIAGE ]
                    });
                    expect(l.hasAttr(LineAttr.WHEEL_CHAIR_ACCESS)).toBe(false);
                    expect(l.hasAttr(LineAttr.LINE_AIRPORT)).toBe(false);
                    expect(l.hasAttr(LineAttr.BICYCLE_CARRIAGE)).toBe(true);
                    
                    l = new Line({});
                    expect(l.hasAttr(LineAttr.WHEEL_CHAIR_ACCESS)).toBe(false);
                    expect(l.hasAttr(LineAttr.LINE_AIRPORT)).toBe(false);
                    expect(l.hasAttr(LineAttr.BICYCLE_CARRIAGE)).toBe(false);
                });
                
            });

        });
        
    });
    
});
