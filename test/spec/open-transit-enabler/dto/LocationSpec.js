define(function(require) {
    
    describe('Location', function() {
        
        beforeEach(function() {
            Location = require('ote/dto/Location');
            LocationType = require('ote/dto/LocationType');
            Product = require('ote/dto/Product');
        });
        
        describe('constructor', function() {
            
            it('should accept config with Point value and build coord', function() {
                l = new Location({
                    point: new Point(10,2.564)
                });
                expect(l._lat).toBe(10);
                expect(l._lon).toBe(2.564);
                expect(l._id).toBe(null);
                expect(l._type).toBe(LocationType.COORD);
            });
            
            it('should accept config with lat and lon value and build coord', function() {
                l = new Location({
                    lat: 10,
                    lon: 2.564
                });
                expect(l._lat).toBe(10);
                expect(l._lon).toBe(2.564);
                expect(l._id).toBe(null);
                expect(l._type).toBe(LocationType.COORD);
            });
            
            it('should accept config with only type and id and set lat and lon to null', function() {
                l = new Location({
                    type: LocationType.STATION,
                    id: "station1"
                });
                expect(l._id).toBe("station1");
                expect(l._type).toBe(LocationType.STATION);
                expect(l._lat).toBe(null);
                expect(l._lon).toBe(null);
            });
            
            it('should accept config with all values', function() {
                l1 = new Location({
                    type: LocationType.STATION,
                    id: "station1",
                    lat: 10,
                    lon: 2.564,
                    place: "place",
                    name: "name"
                    /*products: [
                        new Product(); //TODO!
                    ]*/
                });
                expect(l1._type).toBe(LocationType.STATION);
                expect(l1._id).toBe("station1");
                expect(l1._lat).toBe(10);
                expect(l1._lon).toBe(2.564);
                expect(l1._place).toBe("place");
                expect(l1._name).toBe("name");
                
                l2 = new Location({
                    type: LocationType.STATION,
                    id: "station1",
                    point: new Point(10, 2.564),
                    place: "place",
                    name: "name"
                    /*products: [
                        new Product(); //TODO!
                    ]*/
                });
                expect(l2._type).toBe(LocationType.STATION);
                expect(l2._id).toBe("station1");
                expect(l2._lat).toBe(10);
                expect(l2._lon).toBe(2.564);
                expect(l2._place).toBe("place");
                expect(l2._name).toBe("name");
                
                expect(l1).toEqual(l2);
            });
            
            it('should not accept null type', function() {
                expect(function() {
                    new Location({
                        id: "test1",
                        type: null
                    });
                } ).toThrowError(TypeError);
            });
            
            it('should not accept empty string as id', function() {
                expect(function() {
                    new Location({
                        id: "",
                        lat: 0,
                        lon: 0
                    });
                } ).toThrowError(TypeError);
            });
            
            it('should not accept place without name', function() {
                expect(function() {
                    new Location({
                        id: "1",
                        lat: 0,
                        lon: 0,
                        place: "Test"
                    });
                } ).toThrowError(TypeError);
            });
            
            it('should not accept coordinates and ANY type', function() {
                expect(function() {
                    new Location({
                        id: "1",
                        lat: 0,
                        lon: 0,
                        type: LocationType.ANY
                    });
                } ).toThrowError(TypeError);
            });
            
            it('should not accept COORD without lat and lon', function() {
                expect(function() {
                    new Location({
                        id: "1",
                        type: LocationType.COORD
                    });
                } ).toThrowError(TypeError);
            });
            
            it('should not accept COORD with name or place', function() {
                expect(function() {
                    new Location({
                        id: "1",
                        name: "Test",
                        type: LocationType.COORD
                    });
                } ).toThrowError(TypeError);
                expect(function() {
                    new Location({
                        id: "1",
                        place: "Test",
                        type: LocationType.COORD
                    });
                } ).toThrowError(TypeError);
            });
            
        });
        
        describe('functions', function() {
            
            describe('hasId', function() {
                
                it('should return whether location has ID or not', function() {
                    var preset = {
                        lat: 10,
                        lon: 20
                    }
                    
                    l1 = new Location(preset);
                    expect(l1.hasId()).toBe(false);
                    
                    preset.id = "10";
                    l2 = new Location(preset);
                    expect(l2.hasId()).toBe(true);
                });
                
            });
            
            describe('hasLocation', function() {
                
                it('should return whether there are coords given', function() {
                    var preset = {
                        name: "Test",
                        id: "0"
                    }
                    
                    l1 = new Location(preset);
                    expect(l1.hasLocation()).toBe(false);
                    
                    preset.lat = 10;
                    l2 = new Location(preset);
                    expect(l2.hasLocation()).toBe(true);
                    
                    preset.lat = 0;
                    preset.lon = 0.01;
                    l3 = new Location(preset);
                    expect(l3.hasLocation()).toBe(true);
                });
                
            });
            
            describe('hasName', function() {
                
                it('should return whether there is a name or an ID given', function() {
                    preset = {
                        lat: 10,
                        lon: 20
                    }
                    
                    l1 = new Location(preset);
                    expect(l1.hasName()).toBe(false);
                    
                    preset.name = "Test";
                    l2 = new Location(preset);
                    expect(l2.hasName()).toBe(true);
                    
                    preset.id = "Test";
                    l3 = new Location(preset);
                    expect(l3.hasName()).toBe(true);
                    
                    preset.name = null;
                    l4 = new Location(preset);
                    expect(l4.hasName()).toBe(true);
                });
                
            });
            
            describe('isIdentified', function() {
                
                it('should return true for POIs', function() {
                    l = new Location({
                        type: LocationType.POI
                    });
                    expect(l.isIdentified()).toBe(true);
                });
                
                it('should return true for STATIONs with IDs', function() {
                    preset = {
                        type: LocationType.STATION
                    }
                    l1 = new Location(preset);
                    expect(l1.isIdentified()).toBe(false);
                    
                    preset.id = '10x';
                    l2 = new Location(preset);
                    expect(l2.isIdentified()).toBe(true);
                });
                
                it('should return true for ADRESSes or COORDs with a location given', function() {
                    preset = {
                        type: LocationType.ADRESS,
                        name: "Test",
                        lat: null,
                        lon: null
                    }
                    l1 = new Location(preset);
                    expect(l1.isIdentified()).toBe(false);
                    
                    preset.type = LocationType.COORD;
                    l2 = new Location(preset);
                    expect(l2.isIdentified()).toBe(false);
                    
                    preset.lat = 32;
                    preset.lon = 44;
                    l3 = new Location(preset);
                    expect(l3.isIdentified()).toBe(true);
                    
                    preset.type = LocationType.ADRESS;
                    l4 = new Location(preset);
                    expect(l4.isIdentified()).toBe(true);
                });
                
            });
            
            describe('uniqueShortName', function() {
                
                it('should return the name, the ID or null', function() {
                    preset = {
                        lat: 10,
                        lon: 20
                    }
                    
                    l1 = new Location(preset);
                    expect(l1.uniqueShortName()).toBe(null);
                    
                    preset.id = "x10";
                    l2 = new Location(preset);
                    expect(l2.uniqueShortName()).toBe("x10");
                    
                    preset.name = "Test";
                    l3 = new Location(preset);
                    expect(l3.uniqueShortName()).toBe("Test");
                    
                    preset.id = null;
                    l4 = new Location(preset);
                    expect(l4.uniqueShortName()).toBe("Test");
                });
                
            });
            
        });
        
    });
    
});
