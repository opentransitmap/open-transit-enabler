define(function (require) {  
    require('otes/dto/PointSpec'); //complete
    require('otes/dto/LocationSpec'); //complete
    //require('otes/dto/ProductSpec'); //TODO: implement all tests
    require('otes/dto/PositionSpec'); //complete
    //require('otes/dto/StopSpec'); //TODO: implement all tests
    require('otes/dto/LineDestinationSpec'); //complete
    require('otes/dto/LineSpec'); //complete
    require('otes/dto/StyleSpec'); //TODO: implement functions tests
});
