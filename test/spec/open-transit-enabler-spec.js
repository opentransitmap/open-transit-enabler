/*global require */

require.config({
    paths: {
        'jasmine': '../lib/jasmine-2.8.0/jasmine',
        'jasmine-html': '../lib/jasmine-2.8.0/jasmine-html',
        'boot': '../lib/jasmine-2.8.0/boot',
        'otes': 'open-transit-enabler',
        'ote': '../../enabler/open-transit-enabler'
    },
    shim: {
        'jasmine': {
            exports: 'window.jasmineRequire'
        },
        'jasmine-html': {
            deps: ['jasmine'],
            exports: 'window.jasmineRequire'
        },
        'boot': {
            deps: ['jasmine', 'jasmine-html'],
            exports: 'window.jasmineRequire'
        }
    }
});

require(['boot'], function() {
    require(['otes/mainSpec'], function() {
        // Initialize the HTML Reporter and execute the environment (setup by `boot.js`)
        window.onload();
    });
});
