/*global require */

require.config({
    paths: {
        'ote': 'open-transit-enabler'
    }
});

require(['ote/main']);
