define(function () {
    
    _ = function(lat,lon) {
        if (isNaN(parseFloat(lat)) | isNaN(parseFloat(lon))) {
            throw new TypeError("Point latitude and longitude need to be numeric values:" + lat + "," + lon);
        }
        this._lat = lat;
        this._lon = lon;
    };
    
    _.prototype.getLat = function() {
        return this._lat;
    };
    
    _.prototype.getLon = function() {
        return this._lon;
    }
    
    //TODO: not implemented equals and hashCode
    
    _.prototype.toString = function() {
        return this._lat + "/" + this._lon;
    };
    
    return Point = _;
});
