define(function(require) {
    
    Position = require('ote/dto/Position');
    Location = require('ote/dto/Location');
    Line = require('ote/dto/Line');
    
    
    _ = function(plannedTime, predictedTime, line, position, destination, capacity, message) {
        
        if (!(line instanceof Line)) {
            throw new KeyError();
        }
        
        this.plannedTime = plannedTime instanceof Date ? plannedTime : null;
        this.predictedTime = predictedTime instanceof Date ? predictedTime : null;
        this.line = line;
        this.position = position instanceof Position ? position : null;;
        this.destination = destination instanceof Location ? destination : null;;
        this.capacity = capacity instanceof Array ? capacity : null;
        this.message = message instanceof String ? capacity : null;
    }
    
    _.prototype.getTime = function() {
        return this.predictedTime != null ? this.predictedTime : this.plannedTime;
    }
    
    //TODO: not implemented equals, hashCode and TIME_COMPARATOR
    
    _.prototype.toString = function() { //TODO: implement better toString, see Java helper method
        var string = '';
        for (arg in this) {
            string += this.arg + ',';
        }
        return string;
    };
    
    return Departure = _;
    
});
