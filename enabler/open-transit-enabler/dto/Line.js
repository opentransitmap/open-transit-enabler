define(function(require) {
    
    Product = require('ote/dto/Product');
    Style = require('ote/dto/Style');
    LineAttr = require('ote/dto/LineAttr');
    
    //TODO: not implemented static FOOTWAY, TRANSFER, SECOND_CONNECTION, DO_NOT_CHANGE
    
    _ = function(args) {
        
        args = args || {};
        
        if ('product' in args && Object.values(Product).indexOf(args.product) !== -1) {
            this.product = args.product;
        } else {
            this.product = null;
        }
        if ('style' in args && args.style instanceof Style) {
            this.style = args.style;
        } else {
            this.style = null;
        }
        if ('attrs' in args && args.attrs instanceof Array) {
            ok = true;
            for (i=0; i<args.attrs.length; i++) {
                attr = args.attrs[i];
                if (Object.values(LineAttr).indexOf(parseInt(attr)) === -1) {
                    ok = false;
                    break;
                }
            }
            this.attrs = ok ? args.attrs : null;
        } else {
            this.attrs = null;
        }

        this.id = args.id || null;
        this.network = args.network || null;
        this.label = args.label || null;
        this.name = args.name || null;
        this.message = args.message || null;
        
    }
    
    _.prototype.productCode = function() {
        return this.product != null ? this.product : Product.UNKNOWN;
    }

    _.prototype.hasAttr = function(attr) {
        return this.attrs != null && this.attrs.indexOf(attr) !== -1;
    }
    
    //TODO: not implemented equals, hashCode and compareTo
    
    _.prototype.toString = function() { //TODO: implement better toString, see Java helper method
        var string = '';
        for (arg in this) {
            string += this.arg + ',';
        }
        return string;
    };
    
    return Line = _;
    
});
