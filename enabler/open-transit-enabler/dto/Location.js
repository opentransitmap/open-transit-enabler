define(function(require) {
        
        Point = require('ote/dto/Point');
        LocationType = require('ote/dto/LocationType');
        
        _ = function(args) {
                if ('type' in args && args.type !== undefined && args.type in Object.values(LocationType)) { //TODO: from ES2017 > https://stackoverflow.com/questions/17635866/get-values-from-an-object-in-javascript
                        this._type = args.type;
                } else if ('point' in args && args.point instanceof Point ||
                                        ('lat' in args && args.lat !== undefined &&
                                        'lon' in args && args.lon !== undefined ) ) {
                        this._type = LocationType.COORD;
                } else {
                        throw new TypeError();
                }
                if ('id' in args && args.id !== undefined && args.id.length() == 0) {
                        throw new TypeError();
                }
                this._id = args.id || null;
                
                if ('point' in args && args.point instanceof Point) {
                        this._lat = args.point.getLat();
                        this._lon = args.point.getLon();
                } else {
                        this._lat = args.lat || null;
                        this._lon = args.lon || null;
                }
                if ('place' in args && args.place !== undefined && !('name' in args && args.name !== undefined)) {
                        throw new TypeError();
                }
                
                this._place = args.place || null;
                this._name = args.name || null;
                this._products = args.products || null;
                
                //TODO: implement checks!
        }
        
        _.prototype.hasId = function() {
        return !Strings.isNullOrEmpty(this._id); //TODO
    }

    _.prototype.hasLocation = function() {
        return this._lat !== null || this._lon !== null;
    }

    _.prototype.hasName = function() {
        return this._name !== null;
    }
    
    _.prototype.isIdentified = function() {
        if (this._type === LocationType.STATION)
            return this.hasId();

        if (this._type === LocationType.POI)
            return true;

        if (this._type === LocationType.ADDRESS || this._type === LocationType.COORD)
            return this.hasLocation();

        return false;
    }
        
        //TODO: not implemented NON_UNIQUE_NAMES
        
        _.prototype.uniqueShortName = function() {
        if (this._name !== null)
            return _name;
        else if (this.hasId())
            return this._id;
        else
            return null;
    }
    
    //TODO: not implemented equals and equalsAllFields and hashCode
        
        _.prototype.toString = function() { //TODO: implement better toString, see Java helper method
                var string =  this._type + ',' + this._id + ',';
                if (this.hasLocation()) {
                        string += this._lat + "/" + this._lon + ',';
                }
                string += this._place + ',' + this._name + ',' + this._products;
                return string;
        };
        
        return Location = _;
});
