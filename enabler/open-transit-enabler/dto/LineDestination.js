define(function(require) {
    
    Line = require('ote/dto/Line');
    Location = require('ote/dto/Location');
    
    _ = function(line, destination) {
        
        if (line == null || !(line instanceof Line)) {
            throw new TypeError("line has to be Line object and cannot be null");
        }
        
        this.line = line;
        this.destination = destination || null;
        
    }
    
    _.prototype.toString = function() { //TODO: implement better toString, see Java helper method
        return this._line + ": " + this._destination;
    };
    
    return LineDestination = _;
    
});
