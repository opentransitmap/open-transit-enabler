define(function(require) {
    
    FareType = require('ote/dto/FareType');
    Currency = require('ote/dto/Currency'); //TODO: java.util.Currency
    
    
    _ = function(network, type, currency, fare, unitName, units) {
        
        if (!network || Object.values(FareType).indexOf(type) === -1 || !(currency instanceof Currency)
        || !Number.isNumber(fare)) {
            throw new KeyError();
        }
        
        this.network = network;
        this.type = type;
        this.currency = currency;
        this.fare = fare;
        this.unitName = unitName;
        this.units = units;
    }
    
    //TODO: not implemented equals and hashCode
    
    _.prototype.toString = function() { //TODO: implement better toString, see Java helper method
        var string = '';
        for (arg in this) {
            string += this.arg + ',';
        }
        return string;
    };
    
    return Fare = _;
    
});
