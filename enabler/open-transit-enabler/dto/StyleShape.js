define(function(require) {
    
    _ = {
        RECT: 0,
        ROUNDED: 1,
        CIRCLE: 2
    };
    
    return StyleShape = _;
    
});
