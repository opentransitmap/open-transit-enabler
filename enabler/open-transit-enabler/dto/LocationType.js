define(function() {
    
    _ = {
        /** Location can represent any of the below. Mainly meant for user input. */
        ANY : 0,
        /** Location represents a station or stop. */
        STATION : 1,
        /** Location represents a point of interest. */
        POI : 2,
        /** Location represents a postal address. */
        ADDRESS : 4,
        /** Location represents a just a plain coordinate, e.g. acquired by GPS. */
        COORD : 5
    }
    
    return LocationType = _;
});
