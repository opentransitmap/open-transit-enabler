define(function(require) {
        
    StyleColors = require('ote/dto/StyleColors');
    StyleShape = require('ote/dto/StyleShape');

    _ = function(args) {
            
        if (args.backgroundColor == null || args.foregroundColor == null) {
                throw new TypeError("background and fourground colors have to be set");
        }
        
        this.shape = args.Shape || StyleShape.ROUNDED;
        this.backgroundColor = args.backgroundColor;
        this.foregroundColor = args.foregroundColor;
        this.backgroundColor2 = args.backgroundColor2 || null;
        this.borderColor = args.borderColor || null;
            
    }

    _.prototype.hasBorder = function() {
        return this.borderColor !== 0;
    }

    //TODO: not implemented equals, hashCode and compareTo

    _.parseColor = function(colorStr) {
            
        if (!colorStr || colorStr.substr(0,1) != '#') {
                throw new TypeError("Unknown color: " + colorStr);
        }
        
        var color = parseFloat(colorStr.substr(1), 16);
        if (colorStr.length() === 7) {
                color |= 0xff000000; // Amend the alpha value
        }
        return color;
    }

    _.rgb = function(red, green, blue) {
        return (0xFF << 24) | (red << 16) | (green << 8) | blue;
    }

    _.red = function(color) {
        return (color >> 16) & 0xff;
    }

    _.green = function(color) {
        return (color >> 8) & 0xff;
    }

    _.blue = function(color) {
        return color & 0xff;
    }

    _.perceivedBrightness = function(color) {
        // formula for perceived brightness computation: http://www.w3.org/TR/AERT#color-contrast
        return (0.299 * _.red(color) + 0.587 * _.green(color) + 0.114 * _.blue(color)) / 256;
    }

    _.deriveForegroundColor = function(backgroundColor) {
        // dark colors, white font. Or light colors, black font
        if (_.perceivedBrightness(backgroundColor) < 0.5)
            return StyleColors.WHITE;
        else
            return _.Colors.BLACK;
    }
        
        return Style = _;
        
});
