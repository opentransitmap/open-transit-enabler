define(function(require) {
    
    _ = {
        CIRCLE_CLOCKWISE: 0,
        CIRCLE_ANTICLOCKWISE: 1,
        SERVICE_REPLACEMENT: 2,
        LINE_AIRPORT: 3,
        WHEEL_CHAIR_ACCESS: 4,
        BICYCLE_CARRIAGE: 5
    };
    
    return LineAttr = _;
    
});
