define(function(require) {
    
    _ = {
        ADULT: 0,
        CHILD: 1,
        YOUTH: 2,
        STUDENT: 3,
        MILITARY: 4,
        SENIOR: 5,
        DISABLED: 6,
        BIKE: 7
    };
    
    return FareType = _;
    
});
