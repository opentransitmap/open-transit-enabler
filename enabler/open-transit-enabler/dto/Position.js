define(function () {
    
    _ = function(name,section) {
        if (name == null) {
            throw new TypeError("name cannot be null or empty");
        }
        
        this.name = name;
        this.section = section || null;
        //TODO: checkArgument(section == null || section.length() <= 3, "section too long: %s", section);
    };
    
    //TODO: not implemented equals and hashCode
    
    _.prototype.toString = function() {
        var string = this.name;
        if (this.section !== null) {
            string += ',' + this.section;
        }
        return string;
    };
    
    return Position = _;
});
