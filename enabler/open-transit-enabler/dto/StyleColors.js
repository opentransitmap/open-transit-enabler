define(function(require) {
    
    _ = {
        BLACK: 0xFF000000,
        DKGRAY: 0xFF444444,
        GRAY: 0xFF888888,
        LTGRAY: 0xFFCCCCCC,
        WHITE: 0xFFFFFFFF,
        RED: 0xFFFF0000,
        GREEN: 0xFF00FF00,
        BLUE: 0xFF0000FF,
        YELLOW: 0xFFFFFF00,
        CYAN: 0xFF00FFFF,
        MAGENTA: 0xFFFF00FF,
        TRANSPARENT: 0
    };
    
    return StyleColors = _;
    
});
