# OpenTransitEnabler

This is a first attempt to port the Java library [public-transport-enabler](https://github.com/schildbach/public-transport-enabler/) to javascript code so that it's functionality will be available inside JavaScript environments.

The project uses [requireJS](http://requirejs.org/) for more structured code that can be (relatively) easy transfered from Java to JavaScript.

It also uses [Jasmine](https://jasmine.github.io/) for test-driven development. To run the tests simply open [SpecRunner.html](test/SpecRunner.html).

__CAUTION!__ This code is far away from functional! Feel free to enhance or change it as you wish.

The code is licenced under the GNU Affero General Public License. For more detailed information please see the [licence terms](LICENCE).
